# Git merge feature script

Script pour rappel par étapes de commandes pratiques du workflow squash-rebase utile lors de la migration d'une branche feature vers la branche develop.



## Features / Fix

- Rappel par étapes de commandes pratiques du workflow squash-rebase.



## Authors

- [@Bastian.R](https://gitlab.com/Bastian.R/)


## Installation

Pour installer le script :

1- Exécuter git bash en tant qu'administrateur et cloner le projet :
```bash
cd
cd Desktop
git clone https://gitlab.com/Bastian.R/git-merge-feature-script.git
```
2- Copier le fichier dans le dossier C:\Program Files\Git\cmd :
```bash
cd git-merge-feature-script
cp './squash-rebase-workflow' 'C://Program Files//Git//cmd'
```

3- Positionnez-vous dans un de vos projets git et lancer le script : 
```bash
squash-rebase-worflow
```
## Screenshot

![App Screenshot](https://i.ibb.co/X7c3CTT/squash-rebase-worflow-screenshot-1.png)


